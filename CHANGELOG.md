# Snippets Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.1.1] - UNRELEASED

### Changed:

- updated dependencies.
- code linting.
- removed 'snippets-staging.js' after deprecation.
- update database schema.
- change generate script function.

### Added:

- add handle message from bot.
- add start conversation with bot.

## [1.1.0] - 01/07/20

### Added:

- add endPoint to enable/disable snippets.
- add rate limite in snippets queue.

### Changed:

- update snippets message.

## [1.0.11] - 16/06/20

### Changed:

- behaved integration script.
- language fallback.
