![alt quicktext](https://www.quicktext.im/themes/quicktext/assets/dist/images/logo.svg 'Quicktext snippets project')

![npm type definitions](https://img.shields.io/npm/types/typescript)
![Gitlab pipeline status (self-hosted)](https://img.shields.io/gitlab/pipeline/quicktext/snippets?gitlab_url=https://gitlab.satoripop.com)
![JIRA sprint completion](https://img.shields.io/jira/sprint/32?baseUrl=https://colossalfactory.atlassian.net/jira)
![Twitter Follow](https://img.shields.io/twitter/follow/quicktextAI)

# Introduction

> This document is not yet complete neither perfect, Please contribute with more info about the project.

This project is a [Node.js](https://nodejs.org/), [Nestjs](https://nestjs.com/) an application made basically with [Typescript](https://www.typescriptlang.org/). This app is a part of Quicktext™ platform and it generates web **snippets**, **FAQ** and other **SEO** data.

## Browser Support

| ![Chrome](https://raw.github.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png) | ![Firefox](https://raw.github.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png) | ![Safari](https://raw.github.com/alrra/browser-logos/master/src/safari/safari_48x48.png) | ![Opera](https://raw.github.com/alrra/browser-logos/master/src/opera/opera_48x48.png) | ![Edge](https://raw.github.com/alrra/browser-logos/master/src/edge/edge_48x48.png) | ![IE](https://raw.github.com/alrra/browser-logos/master/src/archive/internet-explorer_9-11/internet-explorer_9-11_48x48.png) |
| ---------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| Desktop ✔                                                                                | Desktop ✔                                                                                   | Desktop ✔                                                                                | Desktop ✔                                                                             | Desktop ✔                                                                          | 11 ✔                                                                                                                         |
| Mobile ✔                                                                                 | Mobile ✔                                                                                    | Mobile ✔                                                                                 | Mobile ✔                                                                              | Mobile ✔                                                                           | ~~9 ❌~~                                                                                                                     |

## Documentation

A more detailed documentation may be available at [Jira Confluence](https://colossalfactory.atlassian.net/wiki/spaces/QUIC/pages/).

## Requirements

- **Node.js** (LTS) version installed (v10.x Dubnium preferred),
- **yarn** or **npm** installed and up to date,
- **IDE** of your choice with React and Typescript support.

# Contributing

Since you're here, that means you had access to the rest of source code. So, either you are a contributor or reviewer, pull requests are welcome.

For major changes, please open an issue first to discuss what you would like to change.

## Getting started

First, clone the repo via git:

```bash
$ git clone https://gitlab.satoripop.com/quicktext/snippets.git
```

Then, get in the project directory.

```bash
$ cd snippets
```

This package is shipped with an `.env.default` file in the root of the project. You must rename this file to just `.env` and change the variables you see incompatible with your work environment.

**Note**: You may don't need to change anything in almost all cases.

And then install dependencies with yarn.

```bash
$ npm install
$ # OR
$ yarn
```

**Note**: We encourage the use of [yarn](https://github.com/yarnpkg/yarn), because it is already used in some of our custom scripts. Mixing use of `yarn` and `npm` could lead to stability issues.

## Environment variables

The execution contains some external and internal API calls to get some settings from the backend application and some images and audio assets from the CDN. Then, it set up a connection with a socket end-point to exchange messages.

In the project directory, you will find 4 files `.env.default`, `.env.dev`, `.env.staging` and `.env.prod`. Each file contains the proper variables to run the snippets in the concerned environment.

Depending on the environment you want to work in, you have to copy the content of the chosen file (recommended `.env.default`) and paste it inside the newly created file under the name `.env`.

To copy the `.env.default` vars in the `.env file and create it:

```bash
$ cp .env.default .env
```

## Available Scripts

In the project directory, and for development uses, you can run:

### `start:dev`

```bash
$ npm run start:dev
$ # OR
$ yarn start:dev
```

This runs the app in the development mode.<br>
Open [http://localhost:15000](http://localhost:15000) to view it in the browser.

The app will reload if you make edits.<br>
You will also see any lint errors in the console

### `test`

```bash
$ npm run test
$ # OR
$ yarn test
```

Launches the test runner in the interactive watch mode.

### `build`

In the production, you can run:

```bash
$ npm run build
$ # OR
$ yarn build
```

### `start`

```bash
$ npm run start
$ # OR
$ yarn start
```

Builds the app for production to the `dist` folder.<br>
It correctly bundles the application in production mode and optimizes the build for the best performance.

The build is converted to JS and minified.

## Client-side integration

To integrate the snippets in a customer's website, you just need to add this code fragment `<script>` at the end of the page you want to generate snippets on and change the params to meet his credentials and needs:

```html
<script
  type="application/javascript"
  src="PATH_HERE"
  data-license="LICENCE_HERE"
  data-lang="LANGUAGE_HERE"
  data-staging="IS_STAGING_HERE"
></script>
```

Params:

- **src**: the path from where to get the application script, generally `https://snippets.quicktext.im/static/snippets.js` for production and `https://snippets-staging.quicktext.im/static/snippets.js` for staging.
- **data-license**: the customer license.
- **data-lang**: (optional) the language to generate data with, default is English if no language provided.
- **data-staging**: (optional) "true" if in staging mode and "false" or don't add the params at all if not.

## CI/CD

To deploy the snippets in staging or production you just need to push your contribution to the branch pared with the env you want.

This operation runs an automated Gitlab pipeline that contains four principal phases:

- phase **test:** run test scripts,
- phase **build:** build the snippets bundle,
- phase **deploy:** replace static resources in the cloud,
- phase **purge:** handle cache in the cloud.

| Git branch | env         | test | build |                   deploy                   | purge |
| ---------- | ----------- | :--: | :---: | :----------------------------------------: | :---: |
| develop    | development |  ✔   |       |                                            |       |
| staging    | staging     |  ✔   |   ✔   | [✔](http://snippets-staging.quicktext.im/) |   ✔   |
| prod       | production  |  ✔   |   ✔   |    [✔](https://snippets.quicktext.im/)     |   ✔   |
| master     | none        |  ✔   |       |                                            |       |

You will be notified by email if any CI/CD task failed and the pipeline with be stopped without affecting the running instance of the app. You can watch your CI/CD tasks in this [page](https://gitlab.satoripop.com/quicktext/snippets/pipelines).

## Learn More

To learn about the eco-system that runs the app, check out the [React](https://reactjs.org/), [Redux](https://redux.js.org/introduction/getting-started) and [Socket.io](https://socket.io/docs/#What-Socket-IO-is) documentation.

You can learn more about the [Nest framework](https://docs.nestjs.com/).

Why we choose to [build it with Typescript](https://medium.com/@jtomaszewski/why-typescript-is-the-best-way-to-write-front-end-in-2019-feb855f9b164).

A quick [guide](https://about.gitlab.com/2019/07/12/guide-to-ci-cd-pipelines/) to GitLab CI/CD pipelines.
