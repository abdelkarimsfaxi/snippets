FROM node:lts-alpine

WORKDIR /usr/src/app

COPY ./package.json ./package.json
RUN yarn

CMD [ "node", "dist/main" ]
