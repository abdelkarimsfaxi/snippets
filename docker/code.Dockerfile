FROM qtk8s.azurecr.io/snippets_base:latest

COPY . .
RUN yarn
RUN yarn build
