/**
 * Quicktext © 2020, Snippets Generator.
 * AI-powered hotel tools, visit https://quicktext.im.
 *
 * please use https://snippets.quicktext.im/static/snippets.min.js for production use.
 */
var now = new Date().toISOString().substring(11, 23);
try {
  var qts = document.querySelector('script[data-license]');
  fetch(
    qts.dataset.staging === 'true'
      ? 'https://snippets-staging.quicktext.im' +
          '/v2/get/snippets?license=' +
          qts.dataset.license +
          '&lang=' +
          qts.dataset.lang || "en"
      : 'https://snippets.quicktext.im' +
          '/v2/get/snippets?license=' +
          qts.dataset.license +
          '&lang=' +
          qts.dataset.lang || "en"
  )
    .then(function(res) {
      return res.json();
    })
    .then(function(data) {
      if (data && Array.isArray(data)) {
        document.body.insertAdjacentHTML(
          'beforeend',
          '<script type="application/ld+json">{"@context": "https://schema.org","@type": "FAQPage","mainEntity":' +
            JSON.stringify(data) +
            '}</script>'
        );
        console.log(
          '%c[QT][info]%c @ ' + now + ' %cFAQ Snippets generated.',
          'font-weight:bolder;',
          'color:gray;',
          'font-weight:bolder;'
        );
      } else {
        console.log(
          '%c[QT][error]%c @ ' +
            now +
            " %cCan't get FAQ snippet's data! (ERR:002)",
          'color:red;font-weight:bolder;',
          'color:gray;',
          'color:red;font-weight:bolder;'
        );
      }
    })
    .catch(function(e) {
      console.log(
        '%c[QT][error]%c @ ' +
          now +
          " %cCan't generate FAQ snippets! (ERR:003)",
        'color:red;font-weight:bolder;',
        'color:gray;',
        'color:red;font-weight:bolder;'
      );
      console.error(e);
    });
} catch (e) {
  console.log(
    '%c[QT][error]%c @ ' +
      now +
      ' %cFailed to execute snippets script! (ERR:001)',
    'color:red;font-weight:bolder;',
    'color:gray;',
    'color:red;font-weight:bolder;'
  );
  console.error(e);
}
