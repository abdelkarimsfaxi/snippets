import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import configuration from './config/configuration';
import { JobModule } from './job/job.module';
import { ScriptModule } from './script/script.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          load: [configuration],
        }),
        MongooseModule.forRootAsync({
          imports: [ConfigModule],
          useFactory: async (config: ConfigService) => ({
            uri: `mongodb://${config.get('mongoUrl')}:${config.get(
              'mongoPort',
            )}/${config.get('mongoDb')}`,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
          }),
          inject: [ConfigService],
        }),
        JobModule,
        ScriptModule,
        MongooseModule,
      ],
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });
  });
});
