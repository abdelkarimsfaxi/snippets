import { Controller, Get } from '@nestjs/common';
import { AppService, WelcomeBody } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHome(): WelcomeBody {
    return this.appService.getHome();
  }
}
