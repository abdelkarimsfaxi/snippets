import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { AppService } from './app.service';
import { AppController } from './app.controller';
import configuration from './config/configuration';
import { ScriptModule } from './script/script.module';
import { JobModule } from './job/job.module';
import { join } from 'path';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public/'),
      serveRoot: '/static',
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        uri: `mongodb://${config.get('mongoUrl')}:${config.get(
          'mongoPort',
        )}/${config.get('mongoDb')}`,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
      }),
      inject: [ConfigService],
    }),
    JobModule,
    ScriptModule,
    MongooseModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
