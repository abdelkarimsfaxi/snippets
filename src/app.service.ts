import { Injectable } from '@nestjs/common';

export interface WelcomeBody {
  name: string;
  version: string;
  env: string;
}

@Injectable()
export class AppService {
  getHome(): WelcomeBody {
    return {
      name: process.env.npm_package_name,
      version: process.env.npm_package_version,
      env: process.env.NODE_ENV || 'production',
    };
  }
}
