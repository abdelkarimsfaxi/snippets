export default () => {
  return {
    env: process.env.NODE_ENV,
    port: process.env.PORT,
    mongoUrl: process.env.MONGO_URL,
    mongoPort: process.env.MONGO_PORT,
    mongoDb: process.env.MONGO_DB,
    rtUrl: process.env.RT_URL,
    botUrl :process.env.BOT_URL,
    botReplyUrl: process.env.BOT_REPLY_URL,
    appUrl: process.env.APP_URL,
    jobTackTraceLimit: process.env.JOB_STACK_TRACE_LIMIT,
    jobAttempts: process.env.JOB_ATTEMPTS,
    jobDelay: process.env.JOB_DELAY,
    jobTimeout: process.env.JOB_TIME_OUT,
    jobLimiterRate: process.env.JOB_LIMITER_RATE,
    jobLimiterDuration: process.env.JOB_LIMITER_DURATION,
    redis: {
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
    },
  };
};
