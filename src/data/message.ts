export const messages = {
  en: {
    '10-05': {
      display: 'What is the cancelation policy for [hotelName] ?',
      bot: 'What is the cancelation policy',
    },
    '10-20': {
      display: 'What it the amount of the city tax at [hotelName] ?',
      bot: 'What it the amount of the city tax',
    },
    '11-01': {
      display: 'At what time is the check in at [hotelName] ?',
      bot: 'At what time is the check in',
    },
    '11-02': {
      display: 'At what time is the check out at [hotelName] ?',
      bot: 'At what time is the check out',
    },
    '11-03': {
      display: 'May we leave our luggage before check in, at [hotelName] ?',
      bot: 'May we leave our luggage before check in',
    },
    '11-04': {
      display: 'Does the [hotelName] have 24/7 reception?',
      bot: 'Does the hotel have 24/7 reception?',
    },
    '12-05': {
      display: 'What is the breakfast menu at [hotelName] ?',
      bot: 'What is the breakfast menu',
    },
    '13-01': {
      display: 'Is there a parking at [hotelName] ?',
      bot: 'Is there a parking',
    },
    '13-11': {
      display: 'Is there a free wifi at [hotelName] ?',
      bot: 'Is there a free wifi',
    },
    '13-13': {
      display: 'What are the activities around [hotelName] ?',
      bot: 'What are the activities',
    },
    '13-50': {
      display:
        'How can we get to the [hotelName] from the closest airport or train station ?',
      bot:
        'How can we get to the hotel from the closest airport or train station ?',
    },
    '14-01': {
      display: 'How many rooms are there at [hotelName] ?',
      bot: 'How many rooms are there',
    },
    '15-06': {
      display: 'What are the bathrooms equipment at [hotelName] ?',
      bot: '',
    },
    '16-08': {
      display: 'Which language does the staff speaks at [hotelName] ?',
      bot: 'Which language does the staff speaks',
    },
    '16-10': {
      display: 'Are pets (dogs and cats...) allowed at [hotelName] ?',
      bot: 'Are pets allowed',
    },
    '16-11': {
      display: 'What are the hygiene and cleaning measures at [hotelName] ?',
      bot: 'What are the hygiene and cleaning measures',
    },
    '17-10': {
      display: 'Is there a spa at [hotelName] ?',
      bot: 'do you have spa',
    },
    '17-31': {
      display: 'Is there a swimming pool at [hotelName] ?',
      bot: 'do you have swimming pool',
    },
    '17-32': {
      display: 'If there are any gym-facilities at [hotelName] ?',
      bot: 'If there are any gym-facilities',
    },
    '17-44': {
      display: 'Is the [hotelName] fully accessible for wheelchairs ?',
      bot: 'Is the hotel fully accessible for wheelchairs',
    },
    '18-01': {
      display: 'Is there a restaurant in the [hotelName] ?',
      bot: 'do you have restaurant',
    },
    '19-01': {
      display: 'Where exactly is situated the [hotelName] ?',
      bot: 'Where exactly is situated',
    },
    be_reached_fail:
      "Ok! I've forwarded your request to my colleagues and they will get back to you as soon as possible",
    fail: 'Can you please rephrase your request or use the suggestions below?',
  },
  fr: {
    '10-05': {
      display: "Quelle est la politique d'annulation du [hotelName] ?",
      bot: "Quelle est la politique d'annulation",
    },
    '10-20': {
      display: 'Quel est le montant de la taxe de séjour au [hotelName] ?',
      bot: 'Quel est le montant de la taxe de séjour',
    },
    '11-01': {
      display: 'A quelle heure est le check in au [hotelName] ?',
      bot: 'A quelle heure est le check in',
    },
    '11-02': {
      display: 'A quelle heure est le check out au [hotelName] ?',
      bot: 'A quelle heure est le check out',
    },
    '11-03': {
      display:
        "Est il possible de déposer les bagages avant l'enregistrement au [hotelName] ?",
      bot: "Est il possible de déposer les bagages avant l'enregistrement ",
    },
    '11-04': {
      display: 'La reception est elle ouverte 24h/24 au [hotelName] ?',
      bot: 'La reception est elle ouverte 24h/24',
    },
    '12-05': {
      display: 'Quel est le menu du petit déjeuner au [hotelName] ?',
      bot: 'Quel est le menu du petit déjeuner',
    },
    '13-01': {
      display: "[hotelName] possède t'il un parking ?",
      bot: "possède t'il un parking",
    },
    '13-11': {
      display: "[hotelName] possède t'il un Wifi gratuit ?",
      bot: "possède t'il un Wifi gratuit",
    },
    '13-13': {
      display: 'Quelles sont les activités autour du [hotelName] ?',
      bot: 'Quelles sont les activités',
    },
    '13-50': {
      display:
        "Comment se rendre à [hotelName] depuis l'aéroport ou la gare le plus proche ?",
      bot:
        "Comment se rendre à l'hotel depuis l'aéroport ou la gare le plus proche ?",
    },
    '14-01': {
      display: "Combien de chambres y a t'il au [hotelName] ?",
      bot: "Combien de chambres y a t'il",
    },
    '15-06': {
      display: "Quel est l'équipement des salles de bain au [hotelName] ?",
      bot: '',
    },
    '16-08': {
      display:
        'Quelles sont les langues parlées par le personnel du [hotelName] ?',
      bot: 'Quelles sont les langues parlées par le personnel',
    },
    '16-10': {
      display:
        'Les animaux (chiens ou chats) sont-ils acceptés au [hotelName] ?',
      bot: 'Les animaux sont-ils acceptés',
    },
    '16-11': {
      display:
        "Quelles sont les procédure d'hyginène et de nettoyage au [hotelName] ?",
      bot: "Quelles sont les procédure d'hyginène et de nettoyage",
    },
    '17-10': {
      display: "Y a 'il un spa au [hotelName] ?",
      bot: "Y a 'il un spa",
    },
    '17-31': {
      display: "[hotelName] dispose-t-il d'une piscine ?",
      bot: "Y a 'il un piscine",
    },
    '17-32': {
      display: "[hotelName] possède t'il une salle de sport ?",
      bot: "Y a 'il une salle de sport",
    },
    '17-44': {
      display:
        '[hotelName] est-il équipé pour accueillir les personnes handicapées',
      bot: " l'hotel est-il équipé pour accueillir les personnes handicapées",
    },
    '18-01': {
      display: "[hotelName] possède t'il un restaurant ?",
      bot: "Y a 'il une salle de restaurant",
    },
    '19-01': {
      display: 'Quelle est la localisation precise du [hotelName] ?',
      bot: 'Quelle est la localisation precise',
    },
    be_reached_fail:
      "Ok! J'ai transféré votre demande à mes collègues qui reviendront vers vous au plus vite",
    fail:
      'Pouvez-vous reformuler votre demande ou utiliser les suggestions suivantes SVP?',
  },
};
