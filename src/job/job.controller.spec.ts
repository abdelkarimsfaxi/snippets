import { Test, TestingModule } from '@nestjs/testing';
import { JobController } from './job.controller';
import { JobProcessor } from './job.processor';
import { BullModule } from '@nestjs/bull';
import { AppModule } from '../app.module';
import { ScriptModule } from '../script/script.module';

describe('Job Controller', () => {
  let jobController: JobController;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        BullModule.registerQueue({
          name: 'snippets',
        }),
        AppModule,
        ScriptModule,
      ],
      providers: [JobProcessor],
    }).compile();

    jobController = module.get<JobController>(JobController);
  });

  it('should be defined', () => {
    expect(jobController).toBeDefined();
  });
});
