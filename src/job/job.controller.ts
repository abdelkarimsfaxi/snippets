import { InjectQueue } from '@nestjs/bull';
import { Controller, Post, Body, Res } from '@nestjs/common';
import { Queue } from 'bull';
import { ConfigService } from '@nestjs/config';
import { Response } from 'express';
import { NotifDto } from './job.dto';

@Controller()
export class JobController {
  constructor(
    @InjectQueue('snippets') private readonly snippetsQueue: Queue,
    private readonly configService: ConfigService,
  ) {}

  @Post('notify')
  async transcode(@Body() body: NotifDto, @Res() res: Response) {
    if (
      body.dialogs &&
      body.team_licence &&
      body.languages &&
      body.team_id &&
      body.updated &&
      body.team_name
    ) {
      const options = {
        limiter: {
          max: this.configService.get('jobLimiterRate'),
          duration: this.configService.get('jobLimiterDuration'),
          bounceBack: true,
        },
        removeOnComplete: true,
        stackTraceLimit: this.configService.get('jobTackTraceLimit'),
        delay: this.configService.get('jobDelay'),
        timeout: this.configService.get('jobTimeout'),
      };
      await this.snippetsQueue.add(
        'script-job',
        {
          appId: body.team_id,
          languages: body.languages,
          dialogs: body.dialogs,
          team_licence: body.team_licence,
          updated: body.updated,
          team_name: body.team_name,
        },
        options,
      );
      return res.status(200).json('successful');
    } else if (body.hotelName && body.team_id) {
      await this.snippetsQueue.add('script-job', {
        appId: body.team_id,
        hotelName: body.hotelName,
      });
      return res.status(200).json('successful');
    } else {
      return res.status(402).end();
    }
  }
}
