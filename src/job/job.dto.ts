export class NotifDto {
  readonly team_id: number;
  readonly team_licence: string;
  readonly languages: Array<string>;
  readonly dialogs: Array<string>;
  readonly updated: Array<string>;
  readonly hotelName: string;
  readonly team_name: string;
}
