import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { JobController } from './job.controller';
import { JobProcessor } from './job.processor';
import { ScriptModule } from '../script/script.module';

@Module({
  imports: [
    BullModule.registerQueueAsync({
      name: 'snippets',
      useFactory: async (config: ConfigService) => ({
        redis: config.get('redis'),
      }),
      inject: [ConfigService],
    }),
    ScriptModule,
  ],
  controllers: [JobController],
  providers: [JobProcessor],
})
export class JobModule {}
