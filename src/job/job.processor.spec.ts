import { Queue } from 'bull';
import { Test, TestingModule } from '@nestjs/testing';
import { BullModule, getQueueToken } from '@nestjs/bull';
import { JobProcessor } from './job.processor';
import { ScriptModule } from '../script/script.module';
import { AppModule } from '../app.module';

describe('AppProcessor', () => {
  let app: TestingModule;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [
        BullModule.registerQueue({
          name: 'snippets',
        }),
        AppModule,
        ScriptModule,
      ],
      providers: [JobProcessor],
    }).compile();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should returna data', async () => {
    const data = {
      languages: ['en'],
      appId: 401,
      team_name: 'Zalia Staging',
      dialogs: [
        '11-01',
        '11-02',
        '11-03',
        '11-04',
        '12-05',
        '13-01',
        '13-11',
        '13-13',
        '13-50',
        '14-01',
        '15-06',
        '16-08',
        '16-10',
        '16-11',
        '17-10',
        '17-31',
        '17-32',
        '17-44',
        '18-01',
        '19-01',
      ],
    };
    const queue: Queue = app.get<Queue>(getQueueToken('snippets'));
    return await queue.add('script-job', data);
  });
});
