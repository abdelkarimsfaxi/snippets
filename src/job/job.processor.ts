import { Logger } from '@nestjs/common';
import {
  Process,
  OnQueueActive,
  OnQueueEvent,
  BullQueueEvents,
  Processor,
} from '@nestjs/bull';
import { Job } from 'bull';

import { ScriptService } from '../script/script.service';

@Processor('snippets')
export class JobProcessor {
  constructor(private readonly scriptService: ScriptService) {
    this.scriptService = scriptService;
  }
  private readonly logger = new Logger('snippetsQueue');

  @Process('script-job')
  async processTwice(job: Job<any>) {
    this.logger.debug('Start snippets script...');
    if (
      job.data.appId &&
      job.data.languages &&
      job.data.dialogs &&
      job.data.team_licence &&
      job.data.updated &&
      job.data.team_name
    ) {
      await this.scriptService.generateScript(
        job.data.appId,
        job.data.languages,
        job.data.dialogs,
        job.data.team_licence,
        job.data.updated,
        job.data.team_name,
      );
      this.logger.debug('Script completed');
      return job.data;
    } else if (job.data.appId && job.data.hotelName) {
      await this.scriptService.updateHotelName(
        job.data.appId,
        job.data.hotelName,
      );
      this.logger.debug('Script completed');
      return job.data;
    } else {
      this.logger.debug(
        `Processing job ${job.id} of type ${job.name} is exiting in appId : ${job.data.appId}`,
      );
    }
  }

  @OnQueueActive()
  onActive(job: Job) {
    this.logger.log(
      `Processing... job ${job.id} of type ${job.name} in appId : ${job.data.appId}`,
    );
  }

  @OnQueueEvent(BullQueueEvents.COMPLETED)
  onCompleted(job: Job) {
    this.logger.log(
      `Completed job ${job.id} of type ${job.name} with result ${job.returnvalue}`,
    );
  }
}
