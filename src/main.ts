import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { urlencoded, json } from 'express';
import * as helmet from 'helmet';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // helmet is a package to add some security headers to the responses
  app.use(helmet());
  // increase the size of the JSON request
  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));
  // enable CORS
  app.enableCors();
  const options = new DocumentBuilder()
    .setTitle('Quicktext Snippets')
    .setDescription('The API description.')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);
  await app.listen(app.get(ConfigService).get('port'));
}

bootstrap().then(async () => {
  const logger = new Logger();
  const app = await NestFactory.create(AppModule);
  logger.debug(
    `Snippets '${app.get(ConfigService).get('env')}' listen on port ${app
      .get(ConfigService)
      .get('port')}`,
  );
});
