import { Test, TestingModule } from '@nestjs/testing';
import { ScriptController } from './script.controller';
import { ScriptService } from './script.service';
import { INestApplication } from '@nestjs/common';
import { AppModule } from '../app.module';
import { ScriptModule } from './script.module';
import { Schema } from './script.model';
import { MongooseModule } from '@nestjs/mongoose';
import * as request from 'supertest';

describe('script Controller', () => {
  let app: INestApplication;
  let scriptController: ScriptController;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forFeature([{ name: 'Snippets', schema: Schema }]),
        ScriptModule,
        AppModule,
      ],
      controllers: [ScriptController],
      providers: [ScriptService],
      exports: [ScriptService],
    }).compile();

    app = module.createNestApplication();
    await app.init();
    scriptController = module.get<ScriptController>(ScriptController);
  });

  it('should be defined', () => {
    expect(scriptController).toBeDefined();
  });

  it(' GET /get/snippets  (get script) ', async () => {
    const appId = 401;
    await request(app.getHttpServer())
      .get(`/get/snippets?appId=${appId}`)
      .expect(200);
  });
});
