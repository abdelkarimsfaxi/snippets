import { Controller, Query, Get, Res, Post, Body } from '@nestjs/common';
import { Response } from 'express';

import { ScriptService } from './script.service';

@Controller()
export class ScriptController {
  constructor(private readonly scriptService: ScriptService) {}

  @Get('get/snippets')
  async getScript(@Query('appId') appId: number, @Res() res: Response) {
    const script = await this.scriptService.getScript(appId);
    if (script) {
      return res.status(200).json(script);
    } else {
      return res
        .status(401)
        .json(
          `[INFO] Sorry, you have no script in the database for this hotelId: ${appId}`,
        );
    }
  }

  @Get('/v2/get/snippets')
  async getClientScript(
    @Query('license') license: number,
    @Query('lang') lang: string,
    @Res() res: Response,
  ) {
    const script = await this.scriptService.getClientScript(license, lang);
    if (script) {
      return res.status(200).json(script);
    } else {
      return res
        .status(401)
        .json(
          `[INFO] Sorry, snippets is disabled or you have no script in the database for this license: ${license}`,
        );
    }
  }

  @Post('notify/state')
  async updateState(
    @Body('state') state: boolean,
    @Body('appId') appId: number,
    @Res() res: Response,
  ) {
    if (appId) {
      const snippets_state = state && state === true ? true : false;
      const result = await this.scriptService.updateState(
        appId,
        snippets_state,
      );
      if (result) {
        return res.status(200).json({ updated: snippets_state });
      } else {
        return res.status(401).json({ updated: false });
      }
    }
  }

  @Get('get/state')
  async getState(@Query('appId') appId: number, @Res() res: Response) {
    if (appId) {
      const result = await this.scriptService.checkState(appId);
      if (result) {
        return res.status(200).json({ state: true });
      } else {
        return res.status(200).json({ state: false });
      }
    }
  }

  @Post('get/message')
  async handleMessageFromBot(
    @Body() body: any,
    @Res() res: Response,
  ) {
   if(body && body.text && body.conversation && body.conversation.id){
    console.log(body.conversation.id);
    await this.scriptService.handleMessageFromBot(body);
    return res.status(200).json({ state: true });
   }else{
    return res
    .status(401)
    .json(
      `Error when get message from Bot: ${body}`,
    );
   }
  }
}
