import * as mongoose from 'mongoose';

export const Schema = new mongoose.Schema({
  snippets_id: { type: String, required: true, unique: true },
  appId: { type: Number, required: true },
  lang: { type: String, required: true },
  FAQ: { type: Array,required: true, items:[ {type: String}, {type: String}, {type: String} ] },
  team_licence: { type: String, required: true },
  team_name: { type: String, required: true },
  is_enabled: { type: Boolean, required: true, default: false },
});

export interface Script {
  snippets_id: string;
  appId: number;
  lang: string;
  FAQ: object;
  team_licence: string;
  team_name: string;
  is_enabled: boolean;
}

export interface FAQ {
  dialogId : string;
  answer : string;
  botConvId: string;
}
