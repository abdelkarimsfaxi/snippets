import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ScriptController } from './script.controller';
import { ScriptService } from './script.service';
import { Schema } from './script.model';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Snippets', schema: Schema }])],
  controllers: [ScriptController],
  providers: [ScriptService],
  exports: [ScriptService],
})
export class ScriptModule {}
