import { Test, TestingModule } from '@nestjs/testing';
import { ScriptService } from './script.service';
import { AppModule } from '../app.module';
import { ScriptModule } from './script.module';
import { Schema } from './script.model';
import { MongooseModule } from '@nestjs/mongoose';

describe('ScriptService', () => {
  let scriptService: ScriptService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forFeature([{ name: 'Snippets', schema: Schema }]),
        AppModule,
        ScriptModule,
      ],
      providers: [ScriptService],
      exports: [ScriptService],
    }).compile();

    scriptService = module.get<ScriptService>(ScriptService);
  });

  it('should be defined', () => {
    expect(scriptService).toBeDefined();
  });

  it('scriptService (handle script)', async () => {
    const appId = 401;
    await scriptService.getScript(appId, '');
    //expect(result).toHaveProperty('en');
  });
});
