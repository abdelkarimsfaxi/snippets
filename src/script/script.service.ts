import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ConfigService } from '@nestjs/config';
import * as qs from 'query-string';
import { Model } from 'mongoose';
import axios from 'axios';

import { Script } from './script.model';
import { messages } from '../data/message';

export class ScriptService {
  script: Script[] = [];
  constructor(
    @InjectModel('Snippets') private scriptModel: Model<Script>,
    private readonly configService: ConfigService,
  ) {}

  async saveScript(
    snippets_id: string,
    appId: number,
    lang: string,
    FAQ: object,
    team_licence: string,
    team_name: string,
  ) {
    try {
      await this.scriptModel
        .findOneAndUpdate(
          { snippets_id: snippets_id },
          {
            snippets_id,
            appId,
            lang,
            FAQ,
            team_licence,
            team_name,
          },
          {
            upsert: true,
            useFindAndModify: false,
          },
        )
        .exec();
    } catch (err) {
      console.error(err);
    }
  }

  async getScript(appId) {
    try {
      const result = await this.scriptModel
        .find({
          appId: appId,
        })
        .exec();
      let response = {};
      console.log(result);
      if (result && result.length) {
        result.forEach(el => {
          response[el.lang] = [];
          for (let field of el.FAQ) {
            if (field.answer && field.dialogId) {
              const row = {
                '@type': 'Question',
                name: messages[el.lang][field.dialogId].display,
                acceptedAnswer: {
                  '@type': 'Answer',
                  text: field.answer,
                },
              };
              response[el.lang].push(row);
            }
          }
        });
        return JSON.parse(
          JSON.stringify(response)
            .split('[hotelName]')
            .join(result[0].team_name),
        );
      } else {
        console.error(
          `Error when loading snippets script from database in hotelId: ${appId}`,
        );
        return false;
      }
    } catch (err) {
      console.error(err);
    }
  }

  async getClientScript(licence, lang) {
    try {
      const result = await this.scriptModel
        .findOne({
          team_licence: licence,
          lang: lang,
        })
        .exec();
      if (result && result.FAQ && result.FAQ.length) {
        let response = [];
        for (let field of result.FAQ) {
          if (field.answer) {
            const row = {
              '@type': 'Question',
              name: messages[result.lang][field.dialogId].display,
              acceptedAnswer: {
                '@type': 'Answer',
                text: field.answer,
              },
            };
            response.push(row);
          }
        }
        return response;
      } else {
        return false;
      }
    } catch (err) {
      console.error(err);
    }
  }

  async getScriptById(id) {
    try {
      const result = await this.scriptModel
        .find({
          snippets_id: id,
        })
        .exec();
      return result && result[0] && result[0].FAQ ? result[0].FAQ : false;
    } catch (err) {
      console.error(err);
    }
  }

  async generateScript(
    appId,
    languages,
    dialogs,
    team_licence,
    updated,
    team_name,
  ) {
    if (dialogs.length && updated.length) {
      for (let lang of languages) {
        const data = await this.getScriptById(`${appId}-${lang}`);
        for (let dialogId of dialogs) {
          const text = messages[lang][dialogId].bot;
          let oldData;
          if (data) {
            const result = data.filter(el => el && el.dialogId === dialogId);
            if (result && result[0]) {
              oldData = result[0];
            }
          }
          if (
            (data && updated.includes(dialogId)) ||
            (data && !oldData) ||
            !data
          ) {
            await this.startConversation(
              appId,
              dialogId,
              text,
              lang,
              team_licence,
              team_name,
            );
          }
        }
      }
    } else {
      console.error('There is no active snippet dialog');
    }
  }

  async startConversation(
    appId,
    dialogId,
    text,
    lang,
    team_licence,
    team_name,
  ) {
    const url = `${this.configService.get(
      'botUrl',
    )}/api/directlineless/message`;
    const body = {
      conversationId: '',
      text: text,
      serviceUrl: `${this.configService.get('botReplyUrl')}/get/message`,
      params: {
        teamId: appId,
        source: 'SNIPPETS',
      },
    };
    try {
      const result = await axios.post(url, body);
      if (result.status == 200 || result.status == 201) {
        if (result.data && result.data.conversationId) {
          return await this.saveConversationId(
            appId,
            result.data.conversationId,
            `${appId}-${lang}`,
            dialogId,
            team_licence,
            team_name,
            lang,
          );
        } else {
          console.error(
            `[Error] no conversation id in ${this.configService.get(
              'snippetsUrl',
            )}/get/message`,
          );
          return false;
        }
      } else {
        console.error(
          `Error when get conversation id from bot in hotelId: ${appId} and dialogId: ${dialogId}`,
        );
      }
    } catch (err) {
      console.error(err);
    }
  }

  async updateHotelName(appId, hotelName) {
    try {
      const result = await this.scriptModel
        .updateMany(
          {
            appId: appId,
          },
          { team_name: hotelName },
          {
            upsert: true,
            useFindAndModify: false,
          },
        )
        .exec();
      return result;
    } catch (err) {
      console.error(err);
    }
  }

  async updateState(appId, state) {
    try {
      const script = await this.getScript(appId);
      if (script) {
        const result = await this.scriptModel
          .updateMany(
            { appId },
            {
              is_enabled: state,
            },
            {
              upsert: true,
              useFindAndModify: false,
            },
          )
          .exec();
        return result;
      } else {
        const result = await this.scriptModel
          .updateMany(
            { appId },
            {
              is_enabled: state,
              snippets_id: `${appId}-en`,
            },
            {
              upsert: true,
              useFindAndModify: false,
            },
          )
          .exec();
        return result;
      }
    } catch (err) {
      console.error(err);
    }
  }

  async checkState(appId) {
    try {
      const result = await this.scriptModel
        .find({
          appId,
        })
        .exec();
      return result && result[0] && result[0].is_enabled
        ? result[0].is_enabled
        : false;
    } catch (err) {
      console.error(err);
    }
  }

  async saveConversationId(
    appId,
    convId,
    snippetsId,
    dialogId,
    team_licence,
    team_name,
    lang,
  ) {
    const newFieldFromBot = {
      answer: null,
      dialogId: dialogId,
      botConvId: convId,
    };
    let FAQ = await this.getScriptById(snippetsId);
    if (FAQ && FAQ.length) {
      const shouldBeReplaced = FAQ.find(el => el.dialogId === dialogId);
      if (shouldBeReplaced) {
        for (let i = 0; i < FAQ.length; i++) {
          if (FAQ[i].dialogId && FAQ[i].dialogId === dialogId) {
            newFieldFromBot.answer = FAQ[i].answer;
            FAQ[i] = newFieldFromBot;
            break;
          }
        }
      } else {
        FAQ.push(newFieldFromBot);
      }
    } else {
      FAQ = [
        {
          answer: null,
          dialogId: dialogId,
          botConvId: convId,
        },
      ];
    }
    try {
      await this.scriptModel
        .findOneAndUpdate(
          { snippets_id: snippetsId },
          {
            FAQ,
            team_licence,
            team_name,
            lang,
            appId,
          },
          {
            upsert: true,
            useFindAndModify: false,
          },
        )
        .exec();
    } catch (err) {
      console.error(err);
    }
  }

  async handleMessageFromBot(response) {
    const oops = 'Oops. Something went wrong! Try to rephrase.';
    let warringMessage = [oops];
     Object.keys(messages).forEach((index,value) => {
       warringMessage.push(messages[index]['fail']);
       warringMessage.push(messages[index]['be_reached_fail']);
    });
    console.log(response.text);
    if(!warringMessage.includes(response.text)){
    try {
      const result = await this.scriptModel
        .findOneAndUpdate(
          { 'FAQ.botConvId':response.conversation.id },
            {'$set':
              {'FAQ.$.answer':response.text, 'FAQ.$.botConvId': ''}
            },
          {
            upsert: true,
            useFindAndModify: false,
          },
        )
    } catch (err) {
      console.error(err);
    }
  }
}
}
