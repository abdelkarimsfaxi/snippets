import { Injectable } from '@nestjs/common';

@Injectable()
export class RedisClient {
  public client: any;
  constructor() {
    try {
      this.client = require('redis').createClient(
        process.env.REDIS_PORT,
        process.env.REDIS_URL,
      );
    } catch (err) {
      console.log(err);
    }
  }
}
